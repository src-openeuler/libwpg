Name:             libwpg
Version:          0.3.4
Release:          1
Summary:          A library for import of WordPerfect Graphics images
License:          LGPL-2.1-or-later OR MPL-2.0
URL:              https://libwpg.sourceforge.net/
Source0:          https://download.sourceforge.net/libwpg/libwpg-%{version}.tar.xz
BuildRequires: gcc make
BuildRequires: pkgconfig(librevenge-0.0) >= 0.0.1
BuildRequires: pkgconfig(librevenge-generators-0.0)
BuildRequires: pkgconfig(librevenge-stream-0.0)
BuildRequires: pkgconfig(libwpd-0.10)
BuildRequires: boost-devel
BuildRequires: /usr/bin/doxygen
BuildRequires: /usr/bin/help2man
Provides:         libwpg-tools = %{version}-%{release}
Obsoletes:        libwpg-tools < %{version}-%{release}

%description
Libwpg is a collection of library and tools to work with graphics in WPG (WordPerfect Graphics) format.
WPG is the format used among others in Corel sofware, such as WordPerfect(tm) and Presentations(tm).
Supports converting WordPerfect graphic images to other formats.

%package          devel
Summary:          Development files for libwpg
Requires:         libwpg = %{version}-%{release}

%description      devel
The libwpg-devel package contains the following libraries and header files: Develop applications using libwpg.

%package          help
Summary:          Help documents for libwpg
BuildArch:        noarch
Provides:         libwpg-doc = %{version}-%{release}
Obsoletes:        libwpg-doc < %{version}-%{release}

%description      help
The libwpg-help package contains manual pages and API documentation for libwpg.

%prep
%autosetup -p1

%build
%configure --disable-static --disable-silent-rules
%disable_rpath
%make_build

%install
%make_install
%delete_la

export LD_LIBRARY_PATH=%{buildroot}%{_libdir}${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
help2man -N -S 'libwpg %{version}' -o wpg2raw.1 %{buildroot}%{_bindir}/wpg2raw
help2man -N -S 'libwpg %{version}' -o wpg2svg.1 %{buildroot}%{_bindir}/wpg2svg
help2man -N -S 'libwpg %{version}' -n 'batch convert WordPerfect Graphics files into SVG' --help-option=-h \
         --version-string='wpg2svgbatch.pl %{version}' -o wpg2svgbatch.pl.1 ./src/conv/svg/wpg2svgbatch.pl

install -m 0755 -d %{buildroot}/%{_mandir}/man1
install -m 0644 wpg2*.1 %{buildroot}/%{_mandir}/man1

%files
%license COPYING.LGPL COPYING.MPL
%{_libdir}/libwpg-0.3.so.*
%{_bindir}/*
%exclude %{_docdir}/libwpg

%files devel
%{_includedir}/libwpg-0.3
%{_libdir}/libwpg-0.3.so
%{_libdir}/pkgconfig/libwpg-0.3.pc

%files help
%doc docs/doxygen/html AUTHORS NEWS
%{_mandir}/man1/*

%changelog
* Sun Nov 03 2024 Funda Wang <fundawang@yeah.net> - 0.3.4-1
- update to 0.3.4

* Thu Dec 12 2019 liujing<liujing144@huawei.com> - 0.3.3-2
- Package init
